import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		this.rng = new Random();
		this.cards = new Card[52];
		
		String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
		int[] values = {1,2,3,4,5,6,7,8,9,10,11,12,13};
		
		int index = 0;
		for(String suit : suits){
			for(int value : values){
				cards[index] = new Card(suit, value);
				index++;
			}
		}
		this.numberOfCards = cards.length;

	}
	public int length(){
		return this.numberOfCards;
	}
	public Card drawTopCard(){
		if(this.numberOfCards > 0){
			this.numberOfCards--;
			return cards[this.numberOfCards];
		} else {
			return null; 
		}
	}
	public String toString(){
		if(this.numberOfCards == 0){
			return "Deck is empty";
		}	
		String deckString = "";
		for(int i = 0; i < this.numberOfCards; i++){
			deckString += this.cards[i].toString() + "\n";
		}
		return deckString;
	}
	public void shuffle(){
		for(int i = 0; i < this.cards.length; i ++){
			//shuffles current card to a random card only considering unshuffled cards
			int randomIndex = rng.nextInt(cards.length - i) + i;
			
			Card currentCard = this.cards[i];
			this.cards[i] = this.cards[randomIndex];
			this.cards[randomIndex] = currentCard;
		}
	}
} 

