public class Card{
	private String suit;
	private int value;
	
	public Card(String suit, int value){
		this.suit = suit;
		this.value = value;
	}
	public String getSuit(){
		return this.suit;
	}
	public int getValue(){
		return this.value;
	}
	public String toString(){
		return value + " of " + suit;
	}
	public double calculateScore(){
		double score = 0;
		if(this.suit.equals("Hearts")){
			score += value + 0.4;
		}
		if(this.suit.equals("Spades")){
			score += value + 0.3;
		}
		if(this.suit.equals("Diamonds")){
			score += value + 0.2;
		}
		if(this.suit.equals("Clubs")){
			score += value + 0.1;
		}
		return score;
	}
}

